<?php

namespace App\Models\Catalogs;

class AddressType extends ModelCatalog
{

    protected $table = 'cat_address_types';

    public $incrementing = false;
    public $timestamps = false;
}